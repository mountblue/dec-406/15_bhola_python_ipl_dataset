"""

    Use to initialize the database

"""

import mysql.connector
import time


# function to connect IPL database
def connect_database():
    my_database = mysql.connector.connect(
        host='127.0.0.1',
        user='ipluser',
        passwd='IPLuser@1',
        database='ipl',
        auth_plugin='mysql_native_password'
    )
    return my_database


# function to create  matches table if not exists
def create_table_match(my_cursor):
    my_cursor.execute("CREATE TABLE IF NOT EXISTS matches ("
                      "id int primary key,"
                      "season int,"
                      "city VARCHAR(100),"
                      "date VARCHAR(12),"
                      "team1 VARCHAR(100),"
                      "team2 VARCHAR(100),"
                      "toss_winner VARCHAR(100),"
                      "toss_decision VARCHAR(100),"
                      "result VARCHAR(100),"
                      "dl_applied int,"
                      "winner VARCHAR(100),"
                      "win_by_runs int,"
                      "win_by_wickets int,"
                      "player_of_match VARCHAR(100),"
                      "venue VARCHAR(100),"
                      "umpire1 VARCHAR(100),"
                      "umpire2 VARCHAR(100),"
                      "umpire3 VARCHAR(255) )")


# function to create deliveries table if not existes
def create_table_deliveries(my_cursor):
    my_cursor.execute("CREATE TABLE IF NOT EXISTS deliveries ("
                      "match_id int,"
                      "inning int, "
                      "batting_team varchar(100), "
                      "bowling_team varchar(100), "
                      "over_ int, "
                      "ball int, "
                      "batsman varchar(100), "
                      "non_striker varchar(100), "
                      "bowler varchar(100), "
                      "is_super_over int, "
                      "wide_runs int, "
                      "bye_runs int, "
                      "legbye_runs int, "
                      "noball_runs int, "
                      "penalty_runs int, "
                      "batsman_runs int, "
                      "extra_runs int, "
                      "total_runs int, "
                      "player_dismissed varchar(100), "
                      "dismissal_kind varchar(100), "
                      "fielder varchar(100),"
                      "FOREIGN KEY (match_id) "
                      "REFERENCES matches(id) ON DELETE CASCADE"
                      ")")


# function to insert data in match table
def insert_data_match(my_cursor, filename_match):
    sql_delete_query_match = 'DELETE from matches WHERE id > 0'
    my_cursor.execute(sql_delete_query_match)

    sql_load_query_match = "LOAD DATA LOCAL INFILE '"+filename_match+"' "\
                           "INTO TABLE matches "\
                           "FIELDS TERMINATED BY ',' "\
                           "ENCLOSED BY '\"' "\
                           "LINES TERMINATED BY '\n' "\
                           "IGNORE 1 ROWS"
    my_cursor.execute(sql_load_query_match)


# function to insert data in deliveries table
def insert_data_deliveries(my_cursor, filename_deliveries):
    sql_delete_query_delivery = 'DELETE from deliveries WHERE match_id > 0'
    my_cursor.execute(sql_delete_query_delivery)

    sql_load_query_delivery = "LOAD DATA LOCAL INFILE '"\
                              ""+filename_deliveries+"' "\
                              "INTO TABLE deliveries "\
                              "FIELDS TERMINATED BY ',' "\
                              "ENCLOSED BY '\"' "\
                              "LINES TERMINATED BY '\n' "\
                              "IGNORE 1 ROWS"
    my_cursor.execute(sql_load_query_delivery)


# the main function where program starts
def execute():

    start_time = time.time()
    # opening database
    my_database = connect_database()
    my_cursor = my_database.cursor()

    # creating table
    create_table_match(my_cursor)

    filename_match = './../res/matches.csv'

    insert_data_match(my_cursor, filename_match)

    # creating table
    create_table_deliveries(my_cursor)

    filename_deliveries = './../res/deliveries.csv'
    insert_data_deliveries(my_cursor, filename_deliveries)

    # committing the changes and close the database
    my_database.commit()
    my_database.close()
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)

execute()
