"""
    Q.4. For the year 2015 plot the top economical bowlers.
"""

from matplotlib import pyplot as plt
import time
import csv


# Main method
def execute():

    start_time = time.time()

    filename_match = './../res/matches.csv'
    filename_delivery = './../res/deliveries.csv'
    year = '2015'

    match_id_2015 = get_match_id_by_year(filename_match, year)

    bowlers_total_runs = get_total_runs(filename_delivery, match_id_2015)

    bowlers_economical = get_bowler_economical(bowlers_total_runs)

    bowlers_economical_sorted_list = sorted(bowlers_economical.items(),
                                            key=lambda kv: kv[1])

    x_axis = []
    y_axis = []
    bowlers_name = []

    top = 5
    for current in range(top):
        bowlers_name.append(bowlers_economical_sorted_list[current][0])
        y_axis.append(bowlers_economical_sorted_list[current][1])

    x_axis = list(range(top))

    print("Plotting data...")
    draw_plot(x_axis, y_axis, bowlers_name, start_time)
    print("Exit...")


# get match id of year
def get_match_id_by_year(filename_match, year):
    match_id_year = []
    # opening csv file
    with open(filename_match, 'r') as csv_file_match:
        matches_reader = csv.DictReader(csv_file_match, delimiter=',')

        for match_row in matches_reader:
            if match_row['season'] == year:
                match_id_year.append(match_row['id'])

    return match_id_year


# get bowler total runs with all runs
def get_total_runs(filename_delivery, match_id_2015):
    bowlers_total_runs = {}
    with open(filename_delivery, 'r') as csv_file_delivery:
        deliveries_reader = csv.DictReader(csv_file_delivery, delimiter=',')
        for delivery in deliveries_reader:
            if delivery['match_id'] in match_id_2015:
                if delivery['bowler'] in bowlers_total_runs.keys():
                    t = bowlers_total_runs[delivery['bowler']]
                    t['balls'] += 1
                    t['runs'] += int(delivery['total_runs'])
                else:
                    bowlers_total_runs[delivery['bowler']] =\
                        {'balls': 1, 'runs': int(delivery['total_runs'])}
    return bowlers_total_runs


# get bowlers economical
def get_bowler_economical(bowlers_total_runs):
    bowlers_economical = {}
    for bowler_name, bowler_row in bowlers_total_runs.items():
        over = round(bowler_row['balls'] / 6)
        economical = bowler_row['runs'] / over

        bowlers_economical[bowler_name] = economical
    return bowlers_economical


# draw plot
def draw_plot(x_axis, y_axis, bowlers_name, start_time):
    plt.ylabel('Economical')
    plt.xlabel('Bowlers')
    plt.title('Top 5 economical bowlers')
    # plt.bar(x_axis, y_axis, color='#FF9DA7', width=0.5)
    color_set = [
         '#4E79A7', '#F28E2B', '#E15759', '#76B7B2',
         '#59A14F', '#EDC948']
    for index in range(len(x_axis)):
        plt.bar(x_axis[index], y_axis[index],
                color=color_set[index], width=0.5)
    plt.xticks(x_axis, bowlers_name, rotation=90)
    plt.tight_layout()
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)
    plt.show()


# calling main method
execute()
