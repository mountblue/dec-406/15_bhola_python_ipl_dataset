"""
    Q.3. For the year 2016 plot the extra runs conceded per bowler.
"""

from matplotlib import pyplot as plt
import time
import csv


# main method
def execute():
    start_time = time.time()

    filename_match = './../res/matches.csv'
    filename_delivery = './../res/deliveries.csv'
    year = '2016'

    match_id_2016 = get_match_id_by_year(filename_match, year)

    bowling_team_extra_runs = get_extra_runs_bowling_team(filename_delivery,
                                                          match_id_2016)

    bowling_team_name = []
    y_axis = []

    for team_name, runs in bowling_team_extra_runs.items():
        bowling_team_name.append(team_name)
        y_axis.append(runs)

    x_axis = list(range(len(bowling_team_name)))

    print("Plotting data...")
    draw_plot(x_axis, y_axis, bowling_team_name, start_time)
    print("Exit...")


# get match id of year
def get_match_id_by_year(filename_match, year):
    match_id_year = []
    # opening csv file
    with open(filename_match, 'r') as csv_file_match:
        matches_reader = csv.DictReader(csv_file_match, delimiter=',')

        for match_row in matches_reader:
            if match_row['season'] == year:
                match_id_year.append(match_row['id'])

    return match_id_year


# get extra runs of every bowler
def get_extra_runs_bowling_team(filename_delivery, match_id_2016):
    bowling_team_extra_runs = {}

    # opening csv file
    with open(filename_delivery, 'r') as csv_file_delivery:
        deliveries_reader = csv.DictReader(csv_file_delivery, delimiter=',')

        for delivery_row in deliveries_reader:
            if delivery_row['extra_runs'] != '0':
                if delivery_row['match_id'] in match_id_2016:
                    if delivery_row['bowling_team'] in\
                       bowling_team_extra_runs.keys():
                        bowling_team_extra_runs[delivery_row['bowling_team']] \
                            += int(delivery_row['extra_runs'])
                    else:
                        bowling_team_extra_runs[delivery_row['bowling_team']] \
                            = int(delivery_row['extra_runs'])
    return bowling_team_extra_runs


# draw plot
def draw_plot(x_axis, y_axis, bowling_team_name, start_time):
    plt.ylabel('Extra runs')
    plt.xlabel('Bowling Team')
    plt.title('Extra runs according to bowling team in 2016')
    plt.bar(x_axis, y_axis, color='#F28E2B', width=0.3)
    plt.xticks(x_axis, bowling_team_name, rotation=90)
    plt.tight_layout()

    plot_figure_manager = plt.get_current_fig_manager()
    # return a tuple of (width, height)
    # print(plot_figure_manager.window.maxsize())

    plot_figure_manager.resize(*plot_figure_manager.window.maxsize())

    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)
    plt.show()


# calling main method
execute()
