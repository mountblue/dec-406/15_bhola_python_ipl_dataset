"""

    Q.5. Discuss a "Story" you want to tell with the given data.
    As with part 1, prepare the data structure and plot with matplotlib.

 """

from matplotlib import pyplot as plt
import mysql.connector
import time


# function to connect database
def connect_database():
    my_database = mysql.connector.connect(
        host='127.0.0.1',
        user='ipluser',
        passwd='IPLuser@1',
        database='ipl',
        auth_plugin='mysql_native_password'
    )
    return my_database


# get all toss of winner
def get_all_tosses(my_cursor):
    my_cursor.execute("SELECT "
                      "CASE WHEN winner=toss_winner "
                      "THEN toss_decision "
                      "ELSE CASE WHEN toss_decision='bat' "
                      "THEN 'field' ELSE 'bat' END END as toss, "
                      "COUNT(*) FROM matches "
                      "WHERE result LIKE 'normal' GROUP BY toss")

    return my_cursor.fetchall()


# return bat bowl percentage
def get_bat_bowl_percenytage(all_tosses):
    bat = 0
    toss_num = 1
    bat_total = all_tosses[bat][toss_num]

    bowl = 1
    bowl_total = all_tosses[bowl][toss_num]

    total_toss = bat_total + bowl_total

    bat_percentage = (bat_total/total_toss) * 100
    bowl_percentage = (bowl_total/total_toss) * 100

    return [bat_percentage, bowl_percentage]


def get_bat_bowl_number_year(my_cursor):
    my_cursor.execute("SELECT season, "
                      "CASE WHEN winner=toss_winner "
                      "THEN toss_decision ELSE "
                      "CASE WHEN toss_decision='bat' "
                      "THEN 'field' ELSE 'bat' END "
                      "END as toss, COUNT(*) FROM matches "
                      "WHERE result LIKE 'normal' "
                      "GROUP BY season, toss "
                      "ORDER BY season")

    return my_cursor.fetchall()


def get_bat_bowl_number_year_dict(bat_bowl_number_year):
    bat_bowl_number_year_dict = {}
    year = 0
    toss = 1
    num = 2
    for row in bat_bowl_number_year:
        if row[year] not in bat_bowl_number_year_dict.keys():
            bat_bowl_number_year_dict[row[year]] = {}
        bat_bowl_number_year_dict[row[year]][row[toss]] = row[num]

    return bat_bowl_number_year_dict


# main method
def execute():
    start_time = time.time()
    my_database = connect_database()
    my_cursor = my_database.cursor()

    all_tosses = get_all_tosses(my_cursor)

    bat_bowl_number_year = get_bat_bowl_number_year(my_cursor)

    bat_bowl_number_year_dict =\
        get_bat_bowl_number_year_dict(bat_bowl_number_year)

    for year, tosses in bat_bowl_number_year_dict.items():
        print('--', year)
        for toss, num in tosses.items():
            print('-----', toss, ' : ', num)

    [bat_percentage, bowl_percentage] = get_bat_bowl_percenytage(all_tosses)

    print('Bat percentae : ', round(bat_percentage, 2), '%')
    print('Bowl percentage : ', round(bowl_percentage, 2), '%')

    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)


execute()
