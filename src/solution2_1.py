"""

    Q.2. Plot a stacked bar chart of matches won of all teams over all
    the years of IPL.

"""

from matplotlib import pyplot as plt
import csv
import time


# Main method
def execute():
    # store time when execution starts
    start_time = time.time()

    filename = './../res/matches.csv'

    [number_of_wins, teams] = get_number_of_wins(filename)

    # for k, v in number_of_wins.items():
    #     print(k)
    #     print(v)

    # print(teams)

    print("Showing plot diagram...")
    draw_plot(number_of_wins, teams, start_time)
    print("Exit...")


# function to return numbers of winning
def get_number_of_wins(filename):
    # opening csv file

    number_of_wins = {}
    player_list = {}
    with open(filename, 'r') as csv_file:
        matches_reader = csv.DictReader(csv_file)

        for match in matches_reader:
            if match['winner'] not in player_list:
                if match['result'] != 'no result':
                    player_list[match['winner']] = 0

        csv_file.seek(0)
        csv_file.__next__()
        for match in matches_reader:
            if match['result'] != 'no result':
                if match['season'] in number_of_wins.keys():
                    number_of_wins[match['season']][match['winner']] += 1
                else:
                    number_of_wins[match['season']] = player_list.copy()
                    number_of_wins[match['season']][match['winner']] = 1

    return [number_of_wins, list(player_list.keys())]


# draw plot on graph
def draw_plot(number_of_wins, teams, start_time):

    bar_width = 0.35
    # color set
    color_set = [
         '#DD3232', '#FBAF5F', '#EC008C', '#F68869',
         '#FFEF6C', '#FFB6AD', '#CED75C', '#D2EFDB',
         '#A1CE5E', '#DBDFC3', '#969A52', '#1A86A8',
         '#985396', '#805462', '#391242', '#58574B',
         '#572C29', '#FFE293'
    ]

    years = list(number_of_wins.keys())
    years.sort()
    number_of_year = len(years)

    y_offset = [0] * number_of_year

    x_axis = list(range(number_of_year))

    score_per_team = {}

    for team in teams:
        score_per_team[team] = []
        for year in years:
            score_per_team[team].append(number_of_wins[year][team])

    plot_bar_list = []

    color_start = 0

    for team in teams:
        y_axis = list(score_per_team[team])
        plot_bar = plt.bar(x_axis, y_axis, bar_width,
                           color=color_set[color_start], bottom=y_offset)
        plot_bar_list.append(plot_bar)
        color_start += 1

        y_offset = [sum(x) for x in zip(*[y_offset, y_axis])]

    plt.title('Matches won of all teams over all the years of IPL')
    plt.ylabel('Teams')
    plt.xlabel('Years')

    plt.xticks(x_axis, years, rotation=90)

    plt.legend(plot_bar_list, teams, fontsize=9, labelspacing=0.5,
               loc='upper right', bbox_to_anchor=(1.18, 1.0))
    plt.tight_layout()
    plot_figure_manager = plt.get_current_fig_manager()
    # return a tuple of (width, height)
    # print(plot_figure_manager.window.maxsize())

    plot_figure_manager.resize(*plot_figure_manager.window.maxsize())

    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)

    plt.show()


# calling main menthod
execute()
