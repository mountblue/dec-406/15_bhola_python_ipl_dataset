"""
    Q.3. For the year 2016 plot the extra runs conceded per team.
"""

from matplotlib import pyplot as plt
import mysql.connector
import time


# main method
def execute():
    start_time = time.time()

    my_database = connect_database()
    my_cursor = my_database.cursor()

    bowling_team_extra_runs = get_extra_runs_bowling_team(my_cursor)

    name = 0
    runs = 1

    for bowling_team in bowling_team_extra_runs:
        print('--', bowling_team[name], ' : ', bowling_team[runs])

    my_database.close()
    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)


# function to connect database
def connect_database():
    my_database = mysql.connector.connect(
        host='127.0.0.1',
        user='ipluser',
        passwd='IPLuser@1',
        database='ipl',
        auth_plugin='mysql_native_password'
    )
    return my_database


# return extra runs of each bowling team
def get_extra_runs_bowling_team(my_cursor):
    my_cursor.execute("SELECT bowling_team, SUM(extra_runs) "
                      "FROM deliveries "
                      "RIGHT JOIN matches ON deliveries.match_id = matches.id "
                      "WHERE matches.season = 2016 "
                      "GROUP BY bowling_team")

    return my_cursor.fetchall()


# calling main method
execute()
