"""

    Q.1. Plot the number of matches played per year of all the years in IPL.

"""

import csv
import time
from matplotlib import pyplot as plt


# function from where program starts
def execute():
    # store time when execution starts
    start_time = time.time()
    filename = './../res/matches.csv'
    year_match = get_year_match(filename)
    year_match.pop('season')

    x_axis = list(year_match.keys())
    y_axis = list(year_match.values())

    print("Showing plot diagram...")
    draw_plot(x_axis, y_axis, start_time)
    print("Exit...")


# function to get total matches accordint to year
def get_year_match(filename):
    with open(filename, 'r') as matches_file:
        matches_reader = csv.reader(matches_file, delimiter=',')
        year_match = {}

        season = 1

        for match in matches_reader:
            if match[season] in year_match.keys():
                year_match[match[season]] += 1
            else:
                year_match[match[season]] = 1

    return year_match


# draw plot on the graph
def draw_plot(x_axis, y_axis, start_time):
    plt.title('Number of matches in each year')
    plt.ylabel('Number of matches')
    plt.xlabel('Year')
    plt.bar(x_axis, y_axis, color='#E15759')
    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)

    plt.show()


# call main method
execute()
