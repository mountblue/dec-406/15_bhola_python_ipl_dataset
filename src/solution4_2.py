"""
    Q.4. For the year 2015 plot the top economical bowlers.
"""

from matplotlib import pyplot as plt
import mysql.connector
import time


# function to connect database
def connect_database():
    my_database = mysql.connector.connect(
        host='127.0.0.1',
        user='ipluser',
        passwd='IPLuser@1',
        database='ipl',
        auth_plugin='mysql_native_password'
    )
    return my_database


# return top 5 economical bowlers
def get_top_5_bowlers_economical(my_cursor):
    my_cursor.execute("SELECT bowler, "
                      "ROUND(SUM(total_runs) / ROUND(COUNT(over_) / 6, 0), 2)"
                      " as economical FROM deliveries "
                      "RIGHT JOIN matches on deliveries.match_id = matches.id "
                      "WHERE matches.season = 2015 "
                      "GROUP BY bowler "
                      "ORDER BY economical LIMIT 5 ")

    return my_cursor.fetchall()


# main method
def execute():
    start_time = time.time()
    my_database = connect_database()
    my_cursor = my_database.cursor()

    top_5_bowlers_economical = get_top_5_bowlers_economical(my_cursor)

    name = 0
    score = 1
    for bowler in top_5_bowlers_economical:
        print('--', bowler[name], ' : ', bowler[score])

    my_database.close()

    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)


# calling main method
execute()
