"""
    Q.1. Plot the number of matches played per year of all the years in IPL.
"""

from matplotlib import pyplot as plt
import time
import mysql.connector


# main method
def execute():
    start_time = time.time()

    # connect database
    my_database = connect_database()
    my_cursor = my_database.cursor()

    year_match = get_year_match(my_cursor)

    x_axis = []
    y_axis = []

    # showing all data
    for result_row in year_match:
        x_axis.append(str(result_row[0]))
        y_axis.append(result_row[1])

    my_database.close()

    print("Showing plot diagram...")
    draw_plot(x_axis, y_axis, start_time)
    print("Exit...")


# function to connect database
def connect_database():
    my_database = mysql.connector.connect(
        host='127.0.0.1',
        user='ipluser',
        passwd='IPLuser@1',
        database='ipl',
        auth_plugin='mysql_native_password'
    )
    return my_database


def get_year_match(my_cursor):
    # fetching the data
    my_cursor.execute("SELECT season, COUNT(id) "
                      "FROM ipl.matches "
                      "GROUP BY season "
                      "ORDER BY season")

    return my_cursor.fetchall()


# draw plot on the graph
def draw_plot(x_axis, y_axis, start_time):
    plt.title('Number of matches in each year')
    plt.ylabel('Number of matches')
    plt.xlabel('Year')
    plt.bar(x_axis, y_axis, color='#E15759')
    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)

    plt.show()


# calling main method
execute()
