"""
    Q.2. Plot a stacked bar chart of matches won of
    all teams over all the years of IPL.
"""

from matplotlib import pyplot as plt
import mysql.connector
import time


# main method
def execute():
    start_time = time.time()

    my_database = connect_database()
    my_cursor = my_database.cursor()

    number_wins_dict = get_number_of_wins(my_cursor)

    for year, team in number_wins_dict.items():
        print('-- ', year)
        for name, score in team.items():
            print('------', name, ' : ', score)

    my_database.close()
    # calculate time of execution
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)


# function to connect database
def connect_database():
    my_database = mysql.connector.connect(
        host='127.0.0.1',
        user='ipluser',
        passwd='IPLuser@1',
        database='ipl',
        auth_plugin='mysql_native_password'
    )
    return my_database


# get number of wins
def get_number_of_wins(my_cursor):
    # fetching the data
    my_cursor.execute("SELECT season, winner, COUNT(season) FROM matches "
                      "WHERE result != 'no result' "
                      "GROUP BY season, winner "
                      "ORDER BY season, winner;")

    number_wins = my_cursor.fetchall()
    number_wins_dict = {}

    year = 0
    team = 1
    score = 2

    for win in number_wins:
        if win[year] not in number_wins_dict.keys():
            number_wins_dict[win[year]] = {}

        number_wins_dict[win[year]][win[team]] = win[score]

    return number_wins_dict


# calling main method
execute()
