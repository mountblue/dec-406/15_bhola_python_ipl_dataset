"""

    Q.5. Discuss a "Story" you want to tell with the given data.
    As with part 1, prepare the data structure and plot with matplotlib.

 """

from matplotlib import pyplot as plt
import time
import csv


# Main method
def execute():

    start_time = time.time()

    filename_match = './../res/matches.csv'

    [toss_decisions_year, all_tosses] = get_toss_decisions(filename_match)

    toss_decisions_year_sorted_list = sorted(toss_decisions_year.items(),
                                             key=lambda kv: kv[0])

    [x_axis, y_axis_bat, y_axis_bowl, years] = \
        get_x_y_axis_labels(toss_decisions_year_sorted_list)

    labels = ['Bat first', 'Bowl First']
    sizes = get_bowl_bat_percentage(all_tosses)

    fig, axs = plt.subplots(nrows=1, ncols=2, constrained_layout=False)

    first_pie = True
    for ax in axs.flatten():
        if first_pie:
            draw_pie(ax, sizes, labels)
            first_pie = False
        else:
            draw_plot(ax, x_axis, y_axis_bat, y_axis_bowl, years)

    plt.tight_layout()
    plot_figure_manager = plt.get_current_fig_manager()
    # return a tuple of (width, height)
    # print(plot_figure_manager.window.maxsize())

    plot_figure_manager.resize(*plot_figure_manager.window.maxsize())
    print("Plotting data...")
    end_time = time.time() - start_time
    print("---------- %s seconds ----------" % end_time)
    plt.show()
    print("Exit...")


# get match id of year
def get_toss_decisions(filename_match):
    all_tosses = {'bat': 0, 'bowl': 0}
    toss_decisions = {}
    with open(filename_match, 'r') as csv_file_match:
        matches_reader = csv.DictReader(csv_file_match, delimiter=',')
        for match_row in matches_reader:
            if match_row['result'] == 'normal':
                if match_row['season'] not in toss_decisions.keys():
                    toss_decisions[match_row['season']] = {'bat': 0, 'bowl': 0}

                toss = match_row['toss_decision']
                if match_row['winner'] != match_row['toss_winner']:
                    if toss == 'bat':
                        toss = 'field'
                    else:
                        toss = 'bat'
                if toss == 'bat':
                    toss_decisions[match_row['season']]['bat'] += 1
                    all_tosses['bat'] += 1
                else:
                    toss_decisions[match_row['season']]['bowl'] += 1
                    all_tosses['bowl'] += 1

    return [toss_decisions, all_tosses]


# get bowl bat percentage
def get_bowl_bat_percentage(all_tosses):
    total = all_tosses['bat'] + all_tosses['bowl']
    bat_percentage = (all_tosses['bat']/total) * 100
    bowl_percentage = 100 - bat_percentage
    return [bat_percentage, bowl_percentage]


# return x axis, y axis, years
def get_x_y_axis_labels(toss_decisions_year_sorted_list):
    x_axis = []
    y_axis_bat = []
    y_axis_bowl = []
    years = []
    for index in range(len(toss_decisions_year_sorted_list)):
        years.append(toss_decisions_year_sorted_list[index][0])
        y_axis_bat.append(toss_decisions_year_sorted_list[index][1]['bat'])
        y_axis_bowl.append(toss_decisions_year_sorted_list[index][1]['bowl'])
        x_axis.append(index)

    return [x_axis, y_axis_bat, y_axis_bowl, years]


# draw pie
def draw_pie(ax, sizes, labels, ):
    ax.pie(sizes, labels=labels, autopct='%1.2f%%',
           shadow=True, startangle=90)
    ax.set_title('Winning Toss percentage')

    # ax1.axis('equal')


# draw plot
def draw_plot(ax, x_axis, y_axis_bat, y_axis_bowl, years):
    # men_means = [20, 35, 30, 35, 27]
    # women_means = [25, 32, 34, 20, 25]
    width = 0.35

    rects1 = ax.bar([x-width/2 for x in x_axis], y_axis_bat, width,
                    color='SkyBlue', label='Bat')
    rects2 = ax.bar([x+width/2 for x in x_axis], y_axis_bowl, width,
                    color='IndianRed', label='Bowl')

    ax.set_ylabel('Count')
    ax.set_xlabel('Year')
    ax.set_title('Toss decisions for winning each year')
    ax.set_xticks(x_axis)
    ax.set_xticklabels(years)
    ax.legend()


# calling main method
execute()
